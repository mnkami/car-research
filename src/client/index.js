import angular from "angular";
import datepickerPopup from "angular-ui-bootstrap/src/datepickerPopup";
import dropdown from "angular-ui-bootstrap/src/dropdown";
/*eslint-disable no-unused-vars*/
import Bootstrap from "bootstrap/dist/css/bootstrap.css";
import Styles from "./style.scss";
import datepickerStyles from "angular-ui-bootstrap/src/datepickerPopup/popup.css";
/*eslint-enable no-unused-vars*/

const baseServerUrl = "http://localhost:8000/";
const URLS = {
  analyseUrl: `${baseServerUrl}analyse`
};

module.exports = angular.module("car-research", [
  datepickerPopup,
  dropdown
])
.controller("CarController", ["$scope", "carService", function($scope, carService) {
  $scope.carInfo = {};
  $scope.car = {
    brand: "Toyota",
    model: "Camry",
    year: new Date(),
    byDate: new Date(),
    condition: "Good",
    startPrice: 23000,
    wasInCrash: false
  };
  $scope.options = {
    datepickerMode: "year",
    maxDate: new Date(),
    minDate: new Date("01-01-1950"),
    showWeeks: true,
    minMode: "month"
  };
  $scope.byDateOptions = {
    datepickerMode: "year",
    minDate: new Date($scope.car.year),
    maxDate: new Date("01-01-2030"),
    showWeeks: true,
    minMode: "month"
  };
  $scope.format = "MMMM yyyy";
  $scope.openYearPicker = () => {
    $scope.datePicker.opened = true;
  };
  $scope.openByDatePicker = () => {
    $scope.byDatePicker.opened = true;
  };

  $scope.datePicker = {
    opened: false
  };
  $scope.byDatePicker = {
    opened: false
  };
  $scope.productionDateChanged = () => {
    $scope.byDateOptions.minDate = new Date($scope.car.year);
  };

  $scope.submitForm = (car) => {
    $scope.carInfo = angular.copy(car);
    carService.send(car).then((newPrice) => {
      $scope.result = `Approx price is about $${newPrice}.`;
    });
  };
  $scope.result = "";
  $scope.conditions = [
    "Perfect",
    "Good",
    "Bad"
  ];
  $scope.changeCondition = (condition) => {
    $scope.car.condition = condition;
  };
}])
.directive("carForm", function() {
  return {
    templateUrl: "templates/car-form.html"
  };
})
.factory("carService", function($http) {
  return {
    send: function(data) {
      return $http.post(URLS.analyseUrl, data).then(({data}) => data.price);
    }
  };
});
