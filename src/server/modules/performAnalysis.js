const CONDITIONS = {
  GOOD: "Good",
  PERFECT: "Perfect",
  BAD: "Bad"
}
const fakeBlockingLoad = (time) => {
  let doNothing = 0;
  const stop = new Date().getTime();
  while(new Date().getTime() < stop + time) {
    doNothing++;
  }
  return doNothing;
};

const calcPriceReduce = ({ brand, condition, model, miles, startPrice, year, byDate, wasInCrash = false }) => {
  //car bought from the dealer, so the price immediately drops by 10%
  let newPrice = startPrice * 0.9;
  //how old is the car?
  const calcToDate = new Date(byDate);
  const prodcutionDate = new Date(year);
  const years = Math.floor((calcToDate - prodcutionDate) / (1000 * 3600 * 24 * 360));
  for (let i = 0; i < years; i++) {
    //each year the car loosing in price by 6%
    newPrice = newPrice * 0.96;
  }
  //wheather the car was in a car accident - 10%
  if (wasInCrash) {
    newPrice = newPrice * 0.9;
  }
  //check car condition
  switch (condition) {
    case CONDITIONS.PERFECT:
      newPrice*=1.05;
      break;
    case CONDITIONS.BAD:
      newPrice*=0.93;
  }

  return {
    price: Math.floor(newPrice)
  };
};

const performAnalysis = (data = {}, callback) => {
  fakeBlockingLoad(100);
  callback(JSON.stringify(
    calcPriceReduce(data)
  ));
};

module.exports = performAnalysis;
