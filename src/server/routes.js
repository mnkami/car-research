const SocketIO = require("socket.io");
const performAnalysis = require("./modules/performAnalysis");

const staticRouting = (server) => {
	server.route([{
		method: "GET",
		path: "/public/{param*}",
		handler: {
				directory: {
						path: "public"
        }
    }
  }, {
		method: "GET",
		path: "/templates/{param*}",
		handler: {
				directory: {
						path: "src/client/templates"
        }
    }
  }, {
		method: "GET",
		path: "/",
		handler: {
				file: {
						path: "src/client/index.html",
						lookupCompressed: true
        }
    }
  }]);
};

const initSocketIO = (server) => {
	const io = SocketIO.listen(server.listener);
	io.sockets.on("connection", (socket) => {
    socket.emit({ msg: "welcome" });
	});
};

const analyseRoute = (server) => {
  server.route({
		method: "POST",
		path: "/analyse",
		handler: function (request, reply) {
      performAnalysis(request.payload, reply);
    }
  });

  server.route({
		method: "GET",
		path: "/load-test",
		handler: function (request, reply) {
      process.send({ cmd: "notifyRequest" });
      performAnalysis({}, reply);
    }
  });
};

module.exports = {
  initSocketIO,
  staticRouting,
  analyseRoute
};
