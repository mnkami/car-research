"use strict";

const Hapi = require("hapi");
const cluster = require("cluster");
const numCPUs = require("os").cpus().length;

const {
	staticRouting,
	initSocketIO,
	analyseRoute
} = require("./routes");

const server = new Hapi.Server();
if (cluster.isMaster) {
	for (var i = 0; i < numCPUs; i++) {
		cluster.fork();
	}
	cluster.on("online", function(worker) {
		console.log("Worker " + worker.process.pid + " is online");
	})
	.on("exit", function(worker, code, signal) {
		console.log("Worker " + worker.process.pid + " died with code: " + code + ", and signal: " + signal);
		console.log("Starting a new worker");
		cluster.fork();
	})
	.on("message", (worker) => {
			console.log("message:", worker.process.pid);
	});
} else {
	server.register(require("inert"), () => {
		server.connection({
			host: "localhost",
			port: 8000
		});
		staticRouting(server);
		initSocketIO(server);
		analyseRoute(server);

		server.start((err) => {
			if (err) {
				throw err;
			}
			/*eslint-disable no-console*/
			console.info("Server running at:", server.info.uri, process.pid );
			/*eslint-enable no-console*/
		});
	});
}
