module.exports = {
    entry: "./src/client/index.js",
    output: {
        path: __dirname,
        filename: "public/bundle.js"
    },
    module: {
        loaders: [
            {
            test: /.js$/,
            loader: "babel-loader",
            exclude: /node_modules/,
            query: {
              presets: ["es2015"]
            }
          },
          {
            test: /\.css$/,
            loaders: ["style", "css"]
          },
          {
            test: /\.scss$/,
            loaders: ["style", "css?sourceMap", "sass?sourceMap"]
          },
          {
            test: /\.png$/,
            loader: "url-loader?limit=100000"
          },
          {
            test: /\.jpg$/,
            loader: "file-loader"
          },
          {
            test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=10000&mimetype=application/font-woff&name=./public/[hash].[ext]"
          },
          {
            test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=10000&mimetype=application/octet-stream&name=./public/[hash].[ext]"
          },
          {
            test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
            loader: "file?name=./public/[hash].[ext]"
          },
          {
            test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=10000&mimetype=image/svg+xml&name=./public/[hash].[ext]"
          }
        ]
    }
};
